package ru.t1.strelcov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.exception.system.IncorrectIndexException;

import java.util.Scanner;

@UtilityClass
public final class TerminalUtil {

    private static final Scanner SCANNER = new Scanner(System.in);

    @NotNull
    public static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    public static Integer nextNumber() {
        @NotNull final String input = SCANNER.nextLine();
        try {
            return Integer.parseInt(input);
        } catch (Exception e) {
            throw new IncorrectIndexException(input);
        }
    }

}
