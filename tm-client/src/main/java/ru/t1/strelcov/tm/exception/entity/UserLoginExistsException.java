package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class UserLoginExistsException extends AbstractException {

    public UserLoginExistsException() {
        super("Error: Login already exists.");
    }

}
