package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getName();

    @NotNull
    String getVersion();

    @NotNull
    String getEmail();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

}
