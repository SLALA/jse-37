package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Task;

@Setter
@Getter
@NoArgsConstructor
public final class TaskFindByNameResponse extends AbstractTaskResponse {

    public TaskFindByNameResponse(@NotNull final Task task) {
        super(task);
    }

}
