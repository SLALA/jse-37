package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Task;

@Setter
@Getter
@NoArgsConstructor
public final class TaskChangeStatusByNameResponse extends AbstractTaskResponse {

    public TaskChangeStatusByNameResponse(@NotNull final Task task) {
        super(task);
    }

}
