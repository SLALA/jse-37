package ru.t1.strelcov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.service.ConnectionService;
import ru.t1.strelcov.tm.service.PropertyService;
import ru.t1.strelcov.tm.util.CheckUtil;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private Connection connection;

    @NotNull
    private ITaskRepository repository;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    User testUser = new User("test2", "test", Role.USER);

    @NotNull
    String testUserId = testUser.getId();

    @NotNull
    final List<Task> tasks = Arrays.asList(
            new Task(testUserId, "pr1"),
            new Task(testUserId, "pr4"),
            new Task(testUserId, "pr3"));

    @SneakyThrows
    @Before
    public void before() {
        connection = connectionService.getConnection();
        repository = new TaskRepository(connection);
        userRepository = new UserRepository(connection);
        try {
            userRepository.add(testUser);
            repository.addAll(tasks);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            connection.close();
            throw e;
        }
    }

    @SneakyThrows
    @After
    public void after() {
        repository.clear(testUserId);
        connection.commit();
        userRepository.remove(testUser);
        connection.commit();
        connection.close();
    }

    @SneakyThrows
    @Test
    public void addTest() {
        @NotNull final Task task = new Task(testUserId, "pr4");
        int size = repository.findAll(testUserId).size();
        Assert.assertFalse(repository.findAll(testUserId).contains(task));
        repository.add(task);
        connection.commit();
        Assert.assertTrue(repository.findAll(testUserId).contains(task));
        Assert.assertEquals(size + 1, repository.findAll(testUserId).size());
    }

    @SneakyThrows
    @Test
    public void addAllTest() {
        @NotNull final List<Task> list = Arrays.asList(new Task(testUserId, "pr1"), new Task(testUserId, "pr2"));
        int size = repository.findAll(testUserId).size();
        Assert.assertFalse(repository.findAll(testUserId).containsAll(list));
        repository.addAll(list);
        connection.commit();
        Assert.assertTrue(repository.findAll(testUserId).containsAll(list));
        Assert.assertEquals(size + list.size(), repository.findAll(testUserId).size());
    }

    @Test
    public void findByIdTest() {
        for (@NotNull final Task task : repository.findAll(testUserId)) {
            Assert.assertTrue(repository.findAll(testUserId).contains(repository.findById(task.getId())));
            Assert.assertEquals(task, repository.findById(task.getId()));
        }
    }

    @SneakyThrows
    @Test
    public void removeByIdTest() {
        @NotNull final Task task = repository.findAll(testUserId).get(0);
        int fullSize = repository.findAll(testUserId).size();
        Assert.assertNotNull(repository.removeById(task.getId()));
        connection.commit();
        Assert.assertFalse(repository.findAll(testUserId).contains(task));
        Assert.assertEquals(fullSize - 1, repository.findAll(testUserId).size());
    }

    @SneakyThrows
    @Test
    public void clearByUserIdTest() {
        @NotNull Task task = repository.findAll(testUserId).get(0);
        @NotNull final List<Task> list = repository.findAll(testUserId);
        int size = list.size();
        int fullSize = repository.findAll().size();
        repository.clear(testUserId);
        connection.commit();
        Assert.assertEquals(0, repository.findAll(testUserId).size());
        Assert.assertEquals(fullSize - size, repository.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final List<Task> list = repository.findAll().stream().filter((i) -> testUserId.equals(i.getUserId())).collect(Collectors.toList());
        Assert.assertTrue(list.containsAll(repository.findAll(testUserId)));
        Assert.assertEquals(list.size(), repository.findAll(testUserId).size());
    }

    @Test
    public void findAllSortedByUserIdTest() {
        for (@NotNull final SortType sortType : SortType.values()) {
            @NotNull final String sortColumn = sortType.getDataBaseName();
            @NotNull final Comparator<Task> comparator = sortType.getComparator();
            @NotNull final List<Task> sortedList = repository.findAll(testUserId, sortColumn);
            Assert.assertEquals(sortedList.stream().sorted(comparator)
                    .collect(Collectors.toList()), sortedList);
            @NotNull List<Task> list = repository.findAll(testUserId);
            Assert.assertTrue(list.containsAll(sortedList));
            Assert.assertEquals(list.size(), sortedList.size());
        }
    }

    @Test
    public void findByIdByUserIdTest() {
        for (@NotNull final Task task : repository.findAll(testUserId)) {
            Assert.assertEquals(task, repository.findById(task.getUserId(), task.getId()));
        }
    }

    @Test
    public void findByNameByUserIdTest() {
        for (@NotNull final Task task : repository.findAll(testUserId)) {
            if (CheckUtil.isEmpty(task.getName())) continue;
            Assert.assertTrue(repository.findAll(testUserId).contains(repository.findByName(task.getUserId(), task.getName())));
            @Nullable final Task foundEntity = repository.findByName(task.getUserId(), task.getName());
            Assert.assertNotNull(foundEntity);
            Assert.assertEquals(task.getName(), foundEntity.getName());
        }
    }

    @SneakyThrows
    @Test
    public void removeByNameByUserIdTest() {
        @NotNull final Task task = repository.findAll(testUserId).get(0);
        Assert.assertNotNull(task.getName());
        int fullSize = repository.findAll(testUserId).size();
        Optional.ofNullable(repository.removeByName(task.getUserId(), task.getName())).ifPresent(p -> Assert.assertFalse(repository.findAll(testUserId).contains(p)));
        connection.commit();
        Assert.assertEquals(fullSize - 1, repository.findAll(testUserId).size());
    }

    @SneakyThrows
    @Test
    public void removeByIdByUserIdTest() {
        @NotNull final Task task = repository.findAll(testUserId).get(0);
        int fullSize = repository.findAll(testUserId).size();
        @NotNull final String id = task.getId();
        Optional.ofNullable(repository.removeById(testUserId, id)).ifPresent(p -> Assert.assertFalse(repository.findAll().contains(p)));
        connection.commit();
        Assert.assertEquals(fullSize - 1, repository.findAll(testUserId).size());
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final String projectId = "testId1";
        @NotNull final Task[] tasks = new Task[]{
                new Task(testUserId, "t1"),
                new Task(testUserId, "t1"),
                new Task(testUserId, "t2")
        };
        for (@NotNull final Task task : tasks) {
            task.setProjectId(projectId);
            repository.add(task);
        }
        for (@NotNull final Task task : repository.findAll(testUserId)) {
            if (!projectId.equals(task.getProjectId())) continue;
            @NotNull final List<Task> list = Arrays.asList(tasks);
            Assert.assertTrue(list.containsAll(repository.findAllByProjectId(task.getUserId(), projectId)));
            Assert.assertEquals(list.size(), repository.findAllByProjectId(task.getUserId(), projectId).size());
        }
    }

    @SneakyThrows
    @Test
    public void removeAllByProjectId() {
        @NotNull final String projectId = "testId1";
        @NotNull final Task[] tasks = new Task[]{
                new Task(testUserId, "t1"),
                new Task(testUserId, "t3"),
                new Task(testUserId, "t2")
        };
        for (@NotNull final Task task : tasks) {
            task.setProjectId(projectId);
            repository.add(task);
        }
        int fullSize = repository.findAll().size();
        repository.removeAllByProjectId(testUserId, projectId);
        connection.commit();
        Assert.assertEquals(0, repository.findAllByProjectId(testUserId, projectId).size());
        Assert.assertEquals(fullSize - tasks.length, repository.findAll().size());
    }

}
