package ru.t1.strelcov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.service.ConnectionService;
import ru.t1.strelcov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Arrays;
import java.util.List;

public class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private Connection connection;

    @NotNull
    private IUserRepository repository;

    @NotNull
    final List<User> users = Arrays.asList(
            new User("test1", "test", Role.USER),
            new User("test2", "test", Role.ADMIN));

    @SneakyThrows
    @Before
    public void before() {
        connection = connectionService.getConnection();
        repository = new UserRepository(connection);
        try {
            for (@NotNull final User user : users) {
                repository.removeByLogin(user.getLogin());
            }
            repository.removeByLogin("test3");
            repository.addAll(users);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            connection.close();
            throw e;
        }
    }

    @SneakyThrows
    @After
    public void after() {
        repository.removeByLogin("test1");
        repository.removeByLogin("test2");
        repository.removeByLogin("test3");
        connection.commit();
        connection.close();

    }

    @SneakyThrows
    @Test
    public void addTest() {
        @NotNull final User user = new User("test3", "pr4");
        int size = repository.findAll().size();
        Assert.assertFalse(repository.findAll().contains(user));
        repository.add(user);
        connection.commit();
        Assert.assertTrue(repository.findAll().contains(user));
        Assert.assertEquals(size + 1, repository.findAll().size());
    }

    @SneakyThrows
    @Test
    public void addAllTest() {
        @NotNull final List<User> list = Arrays.asList(new User("test3", "pr1"));
        int size = repository.findAll().size();
        Assert.assertFalse(repository.findAll().containsAll(list));
        repository.addAll(list);
        connection.commit();
        Assert.assertTrue(repository.findAll().containsAll(list));
        Assert.assertEquals(size + list.size(), repository.findAll().size());
    }

    @Test
    public void findByIdTest() {
        for (@NotNull final User user : repository.findAll()) {
            Assert.assertTrue(repository.findAll().contains(repository.findById(user.getId())));
            Assert.assertEquals(user, repository.findById(user.getId()));
        }
    }

    @SneakyThrows
    @Test
    public void removeByIdTest() {
        @NotNull final User user = users.get(0);
        int fullSize = repository.findAll().size();
        Assert.assertNotNull(repository.removeById(user.getId()));
        connection.commit();
        Assert.assertFalse(repository.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void findByLogin() {
        for (@NotNull final User user : repository.findAll()) {
            Assert.assertTrue(repository.findAll().contains(repository.findByLogin(user.getLogin())));
        }
    }

    @SneakyThrows
    @Test
    public void removeByLogin() {
        @NotNull final User user = users.get(0);
        int fullSize = repository.findAll().size();
        Assert.assertNotNull(repository.removeByLogin(user.getLogin()));
        connection.commit();
        Assert.assertFalse(repository.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void loginExists() {
        for (@NotNull final User user : repository.findAll()) {
            Assert.assertTrue(repository.loginExists(user.getLogin()));
        }
        Assert.assertFalse(repository.loginExists("testNotExistedLogin"));
    }

}
