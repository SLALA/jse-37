package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.AbstractException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService service = new ProjectService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    User testUser = new User("test2", "test", Role.USER);

    @NotNull
    String testUserId = testUser.getId();

    @NotNull
    private static final String USER_ID_UNIQUE = "USER_ID_UNIQUE";

    @NotNull
    private static final String NAME_UNIQUE = "NAME_UNIQUE";

    @NotNull
    final List<Project> projects = Arrays.asList(
            new Project(testUserId, "pr1"),
            new Project(testUserId, "pr4"),
            new Project(testUserId, "pr3"));

    @Before
    public void before() {
        try {
            @NotNull User existedUser = userService.findByLogin(testUser.getLogin());
            userService.removeByLogin(existedUser.getLogin());
            service.clear(existedUser.getId());
        } catch (@NotNull Exception ignored) {
        }
        try {
            userService.removeById(USER_ID_UNIQUE);
            service.clear(USER_ID_UNIQUE);
        } catch (@NotNull Exception ignored) {
        }
        userService.add(testUser);
        service.addAll(projects);
    }

    @After
    public void after() {
        service.clear(testUserId);
        service.clear(USER_ID_UNIQUE);
        userService.removeById(testUser.getId());
    }

    @Test
    public void addTest() {
        @NotNull final Project project = new Project(testUserId, "pr1");
        int size = service.findAll(testUserId).size();
        service.add(null);
        Assert.assertEquals(size, service.findAll(testUserId).size());
        service.add(project);
        Assert.assertEquals(size + 1, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).contains(project));
    }

    @Test
    public void addAllTest() {
        @NotNull final Project[] newProjects = new Project[]{new Project(testUserId, "pr11"), new Project(testUserId, "pr22")};
        @NotNull final ArrayList<Project> list = new ArrayList<>(Arrays.asList(newProjects));
        int size = service.findAll(testUserId).size();
        int listSize = list.size();
        @NotNull final ArrayList<Project> listWithNulls = new ArrayList<>(list);
        listWithNulls.addAll(Arrays.asList(null, null));
        service.addAll(listWithNulls);
        Assert.assertEquals(size + listSize, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).containsAll(list));
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId"));
        for (@NotNull final Project project : service.findAll(testUserId)) {
            Assert.assertTrue(service.findAll(testUserId).contains(service.findById(project.getId())));
            Assert.assertEquals(project, service.findById(project.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId"));
        @NotNull final Project project = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeById(project.getId()));
        Assert.assertFalse(service.findAll(testUserId).contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void updateByIdByUserIdTest() {
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, null, "newName", "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateById(null, id, "newName", "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, id, "", "newDesc"));
            service.updateById(userId, id, "newName", "newDesc");
            Assert.assertEquals("newName", service.findById(userId, id).getName());
            Assert.assertEquals("newDesc", service.findById(userId, id).getDescription());
            Assert.assertNotNull(service.updateById(userId, id, "newName", null));
            Assert.assertNull(service.findById(userId, id).getDescription());
        }
    }

    @Test
    public void updateByNameByUserIdTest() {
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUserId();
            @NotNull final String name = project.getName();
            @Nullable final String newName = "newName";
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(userId, null, newName, "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(null, name, newName, "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(userId, name, null, "newDesc"));
            service.updateByName(userId, name, newName, "newDesc");
            Assert.assertEquals("newDesc", service.findByName(userId, newName).getDescription());
            Assert.assertEquals(newName, service.findById(project.getId()).getName());
        }
    }

    @Test
    public void changeStatusByIdByUserIdTest() {
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(null, id, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(userId, null, status));
                service.changeStatusById(userId, id, status);
                Assert.assertEquals(status, service.findById(userId, id).getStatus());
            }
        }
    }

    @Test
    public void changeStatusByNameByUserIdTest() {
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUserId();
            @NotNull final String name = project.getName();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(null, name, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, status));
                service.changeStatusByName(userId, name, status);
                Assert.assertEquals(status, service.findByName(userId, name).getStatus());
            }
            Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, Status.IN_PROGRESS));
        }
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final Project[] newProjects = new Project[]{new Project(USER_ID_UNIQUE, "pr2"), new Project(USER_ID_UNIQUE, "pr1")};
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null));
        @NotNull final ArrayList<Project> list = new ArrayList<>(Arrays.asList(newProjects));
        @NotNull final String userId = list.get(0).getUserId();
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        Assert.assertEquals(list.size(), service.findAll(userId).size());
        Assert.assertTrue(list.containsAll(service.findAll(userId)));

    }

    @Test
    public void findAllSortedByUserIdTest() {
        @NotNull final Project[] newProjects = new Project[]{new Project(USER_ID_UNIQUE, "pr2"), new Project(USER_ID_UNIQUE, "pr1")};
        @NotNull final ArrayList<Project> list = new ArrayList<>(Arrays.asList(newProjects));
        @NotNull Comparator<Project> comparator = SortType.NAME.getComparator();
        @NotNull String sort = SortType.NAME.name();
        @Nullable String userId = list.get(0).getUserId();
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null, sort));
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        @NotNull final List<Project> sortedList = service.findAll(userId, sort);
        Assert.assertEquals(list.size(), sortedList.size());
        Assert.assertTrue(list.containsAll(sortedList));
        Assert.assertEquals(sortedList.stream().sorted(comparator)
                .collect(Collectors.toList()), sortedList);
    }

    @Test
    public void findByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", "notExistedId"));
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUserId();
            @NotNull final String name = project.getName();
            Assert.assertTrue(service.findAll(testUserId).contains(service.findByName(userId, name)));
            Assert.assertEquals(project.getName(), service.findByName(userId, name).getName());
            Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", name));
            Assert.assertThrows(AbstractException.class, () -> service.findByName(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeByName("notExistedId", null));
        @NotNull final Project project = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeByName(project.getUserId(), project.getName()));
        Assert.assertFalse(service.findAll(testUserId).contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void findByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", "notExistedId"));
        for (@NotNull final Project project : service.findAll(testUserId)) {
            @NotNull final String userId = project.getUserId();
            Assert.assertTrue(service.findAll(testUserId).contains(service.findById(userId, project.getId())));
            Assert.assertEquals(project, service.findById(userId, project.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", project.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId", null));
        @NotNull final Project project = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeById(project.getUserId(), project.getId()));
        Assert.assertFalse(service.findAll(testUserId).contains(project));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void clearByUserIdTest() {
        final String userId = testUserId;
        Assert.assertThrows(AbstractException.class, () -> service.clear(null));
        @NotNull final List<Project> list = service.findAll(userId);
        int size = list.size();
        int fullSize = service.findAll().size();
        service.clear(userId);
        service.clear("notExistedId");
        Assert.assertEquals(0, service.findAll(userId).size());
        Assert.assertEquals(fullSize - size, service.findAll().size());
    }

    @Test
    public void createTest() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, "notExistedId", "descr_new"));
        Assert.assertThrows(AbstractException.class, () -> service.add("notExistedId", null, "descr_new"));
        int size = service.findAll(testUserId).size();
        Assert.assertNotNull(service.add(testUserId, NAME_UNIQUE, "descr_new"));
        Assert.assertEquals(size + 1, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).contains(service.findByName(testUserId, NAME_UNIQUE)));
    }

}
