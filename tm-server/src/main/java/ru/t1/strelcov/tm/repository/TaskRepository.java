package ru.t1.strelcov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    public String getTableName() {
        return "tm_task";
    }

    @SneakyThrows
    @Override
    @NotNull
    public Task fetch(@NotNull final ResultSet result) {
        @NotNull Task task = new Task();
        task.setId(result.getString("id"));
        task.setUserId(result.getString("user_id"));
        task.setName(result.getString("name"));
        task.setDescription(result.getString("description"));
        task.setStatus(Status.valueOf(result.getString("status")));
        task.setCreated(new Date(result.getTimestamp("created").getTime()));
        Optional.ofNullable(result.getTimestamp("start_date")).map(Timestamp::getTime).ifPresent((t) -> task.setDateStart(new Date(t)));
        task.setProjectId(result.getString("project_id"));
        return task;
    }

    @SneakyThrows
    @Override
    public void add(@NotNull final Task task) {
        @NotNull final String sql =
                "INSERT INTO " + getTableName() + "(id, user_id, name, description, status, created, start_date, project_id) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getName());
        statement.setString(4, task.getDescription());
        statement.setString(5, task.getStatus().name());
        statement.setTimestamp(6, new Timestamp(task.getCreated().getTime()));
        statement.setTimestamp(7, Optional.ofNullable(task.getDateStart()).map(Date::getTime).map(Timestamp::new).orElse(null));
        statement.setString(8, task.getProjectId());
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void update(@NotNull final Task task) {
        @NotNull final String sql =
                "UPDATE " + getTableName() + " SET user_id = ?, name = ?, description = ?, status = ?, created = ?, start_date = ?, project_id = ? WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getUserId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getStatus().name());
        statement.setTimestamp(5, new Timestamp(task.getCreated().getTime()));
        statement.setTimestamp(6, Optional.ofNullable(task.getDateStart()).map(Date::getTime).map(Timestamp::new).orElse(null));
        statement.setString(7, task.getProjectId());
        statement.setString(8, task.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> tasksOfProject = findAllByProjectId(userId, projectId);
        tasksOfProject.forEach(this::remove);
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> list = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND project_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) list.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return list;
    }

}
