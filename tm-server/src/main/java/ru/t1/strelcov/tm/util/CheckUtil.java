package ru.t1.strelcov.tm.util;

import org.jetbrains.annotations.Nullable;

public class CheckUtil {

    public static boolean isEmpty(@Nullable final String string) {
        return string == null || string.isEmpty();
    }

}
