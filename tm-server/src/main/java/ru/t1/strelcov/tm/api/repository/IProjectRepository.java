package ru.t1.strelcov.tm.api.repository;

import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {
}
