package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class EmptyProjectIdException extends AbstractException {

    public EmptyProjectIdException() {
        super("Error: Project id is empty.");
    }

}
