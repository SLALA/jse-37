package ru.t1.strelcov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    public AbstractBusinessRepository(@NotNull final Connection connection) {
        super(connection);
    }

    public abstract E fetch(@NotNull final ResultSet resultSet);

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final List<E> list = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) list.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return list;
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final String sortColumnName) {
        @NotNull final List<E> list = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE user_id = ? ORDER BY " + sortColumnName;
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) list.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return list;
    }

    @SneakyThrows
    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND name = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable final E entity;
        if (!resultSet.next()) entity = null;
        else entity = fetch(resultSet);
        statement.close();
        resultSet.close();
        return entity;
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable final E entity;
        if (!resultSet.next()) entity = null;
        else entity = fetch(resultSet);
        statement.close();
        resultSet.close();
        return entity;
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        return removeById(id);
    }

    @Nullable
    @Override
    public E removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByName(userId, name));
        entity.map(AbstractBusinessEntity::getId).ifPresent(this::removeById);
        return entity.orElse(null);
    }

}
