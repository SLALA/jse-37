package ru.t1.strelcov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
