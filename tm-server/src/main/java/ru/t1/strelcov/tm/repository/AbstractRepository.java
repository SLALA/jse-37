package ru.t1.strelcov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    protected AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    public abstract String getTableName();

    public abstract E fetch(@NotNull final ResultSet resultSet);

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final List<E> list = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) list.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return list;
    }

    @Override
    public abstract void add(@NotNull final E entity);

    @Override
    public abstract void update(@NotNull final E entity);

    @Override
    public void addAll(@NotNull final List<E> entities) {
        entities.forEach(this::add);
    }

    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final String sql = "DELETE FROM " + getTableName();
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    @SneakyThrows
    @Override
    public void remove(@NotNull final E entity) {
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, entity.getId());
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable final E entity;
        if (!resultSet.next()) entity = null;
        else entity = fetch(resultSet);
        statement.close();
        resultSet.close();
        return entity;
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
