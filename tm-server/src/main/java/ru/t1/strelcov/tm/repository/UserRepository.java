package ru.t1.strelcov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    public String getTableName() {
        return "tm_user";
    }

    @SneakyThrows
    @Override
    @NotNull
    public User fetch(@NotNull final ResultSet result) {
        @NotNull User user = new User();
        user.setId(result.getString("id"));
        user.setLogin(result.getString("login"));
        user.setPasswordHash(result.getString("password_hash"));
        user.setRole(Role.valueOf(result.getString("role")));
        user.setFirstName(result.getString("first_name"));
        user.setLastName(result.getString("last_name"));
        user.setMiddleName(result.getString("middle_name"));
        user.setEmail(result.getString("email"));
        user.setLocked(result.getBoolean("lock"));
        return user;
    }

    @SneakyThrows
    @Override
    public void add(@NotNull final User user) {
        @NotNull final String sql =
                "INSERT INTO " + getTableName() + "(id, login, password_hash, role, first_name, last_name, middle_name, email, lock) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getRole().name());
        statement.setString(5, user.getFirstName());
        statement.setString(6, user.getLastName());
        statement.setString(7, user.getMiddleName());
        statement.setString(8, user.getEmail());
        statement.setBoolean(9, user.getLocked());
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void update(@NotNull final User user) {
        @NotNull final String sql =
                "UPDATE " + getTableName() + " SET login = ?, password_hash = ?, role = ?, first_name = ?, last_name = ?, middle_name = ?, email = ?, lock = ? WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getRole().name());
        statement.setString(4, user.getFirstName());
        statement.setString(5, user.getLastName());
        statement.setString(6, user.getMiddleName());
        statement.setString(7, user.getEmail());
        statement.setBoolean(8, user.getLocked());
        statement.setString(9, user.getId());
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable final User user;
        if (!resultSet.next()) user = null;
        else user = fetch(resultSet);
        statement.close();
        resultSet.close();
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

    @SneakyThrows
    @Override
    public boolean loginExists(@NotNull final String login) {
        @NotNull final String sql = "SELECT COUNT(1) AS row_number FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final int size = resultSet.getInt("row_number");
        statement.close();
        resultSet.close();
        return size > 0;
    }

}
