package ru.t1.strelcov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.sql.Connection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    public abstract IRepository<E> getRepository(@NotNull final Connection connection);

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return repository.findAll();
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.add(entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void addAll(@Nullable final List<E> list) {
        @Nullable List<E> listNonNull = Optional.ofNullable(list).map(l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList())).orElse(null);
        if (listNonNull == null || listNonNull.size() == 0) return;
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.addAll(listNonNull);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E findById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            return Optional.ofNullable(repository.findById(id)).orElseThrow(EntityNotFoundException::new);
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        try {
            final E entity = Optional.ofNullable(repository.removeById(id)).orElseThrow(EntityNotFoundException::new);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
